<?php
function fab_lecture_deactivation()
{
    // unregister the post type, so the rules are no longer in memory
    unregister_post_type('lecture');
    unregister_taxonomy('class');
    unregister_taxonomy('year');
    // clear the permalinks to remove our post type's rules from the database
    flush_rewrite_rules();
}
register_deactivation_hook(__FILE__, 'fab_lecture_deactivation');
