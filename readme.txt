=== Fab Lectures for WordPress ===
Contributors: fabsere
Donate link: https://fabiopacifici.com/product/open-source-donate/
Tags: posts, lectures, years, classes,
Requires at least: 4.5
Requires PHP: 7.2
Tested up to: 5.5
Stable tag: 0.2.1
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

The Fab Lecture Plugin for WordPress adds a new menu to your admin panel "Lectures" useful to achive your favourite lectures. With this plugin you can add years and classes to your lectures.


== Description ==

The Fab Lectures Plugin customise your WordPress admin panel with a custom "Lectures" menu, useful to achive your favourite lectures, specify a year and or a class you might have followed as part of the lecture.
REST API ready. Designed to work with the official WordPress 2019, 2020 themes and now compatible with other themes. 

Features of the plugin:

*   Adds Lectures Post type to the wordpress admin panel.
*   Adds Year, Class, Category, Tags to the lecture.
*   Custom Lecture templates 
*   Tested up to 5.3
*   Designed to work with the WordPress Twentynineteen, Twenty Twenty and other official themes

== Installation ==

This section describes how to install the plugin and get it working.

1. Upload the plugin to your `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Start adding Lectures to your wordpress Blog.


== Frequently Asked Questions ==

= Does this plugin works with any theme?

This Plugin is designed to work with the Official WordPress 2019, 2020 themes but it's also compatible with any other theme.

= What about gutemberg blocks? =

At present there are no gutemberg blocks defined for this plugin, they will be added in a future release.

== Screenshots ==
1. Admin Panel: Add a new lecture screen.
`assets/screenshot-1.png` 

2. Admin Panel: All lectures screen.
`assets/screenshot-2.png` 

3. Admin Panel: Class taxonomy screen
`assets/screenshot-3.png` 

4. Admin Panel: Year taxonomy screen
`assets/screenshot-4.png` 

5. Customizer: Lectures Menu Items
`assets/screenshot-5.png` 

6. Archive Page: All Lectures 
`assets/screenshot-6.png` 

== Changelog ==

= 0.2 =
* Adds support for WordPress official Twenty Twenty theme
* Adds support for WordPress Twenty Twenty child themes
* Adds support for WordPress Twenty Nineteen child themes
* Adds support for Other official WordPress themes 

= 0.1 =
* First stable release.

== Upgrade Notice ==

= 0.2 =
* Second stable release.

= 0.1 =
First stable release.

== Arbitrary section ==

This plugin Requires WordPress Twentynineteen theme.
