<?php

/**
 * Registers the `class` taxonomy,
 * for use with 'lecture'.
 */
function class_init()
{
	register_taxonomy('class', array('lecture'), array(
		'hierarchical'      => false,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts',
		),
		'labels'            => array(
			'name'                       => __('Classes', 'wpfab'),
			'singular_name'              => _x('Class', 'taxonomy general name', 'wpfab'),
			'search_items'               => __('Search Classes', 'wpfab'),
			'popular_items'              => __('Popular Classes', 'wpfab'),
			'all_items'                  => __('All Classes', 'wpfab'),
			'parent_item'                => __('Parent Class', 'wpfab'),
			'parent_item_colon'          => __('Parent Class:', 'wpfab'),
			'edit_item'                  => __('Edit Class', 'wpfab'),
			'update_item'                => __('Update Class', 'wpfab'),
			'view_item'                  => __('View Class', 'wpfab'),
			'add_new_item'               => __('Add New Class', 'wpfab'),
			'new_item_name'              => __('New Class', 'wpfab'),
			'separate_items_with_commas' => __('Separate Classes with commas', 'wpfab'),
			'add_or_remove_items'        => __('Add or remove Classes', 'wpfab'),
			'choose_from_most_used'      => __('Choose from the most used Classes', 'wpfab'),
			'not_found'                  => __('No Classes found.', 'wpfab'),
			'no_terms'                   => __('No Classes', 'wpfab'),
			'menu_name'                  => __('Classes', 'wpfab'),
			'items_list_navigation'      => __('Classes list navigation', 'wpfab'),
			'items_list'                 => __('Classes list', 'wpfab'),
			'most_used'                  => _x('Most Used', 'class', 'wpfab'),
			'back_to_items'              => __('&larr; Back to Classes', 'wpfab'),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'class',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	));
}

/**
 * Sets the post updated messages for the `class` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `class` taxonomy.
 */
function class_updated_messages($messages)
{

	$messages['class'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __('Class added.', 'wpfab'),
		2 => __('Class deleted.', 'wpfab'),
		3 => __('Class updated.', 'wpfab'),
		4 => __('Class not added.', 'wpfab'),
		5 => __('Class not updated.', 'wpfab'),
		6 => __('Classes deleted.', 'wpfab'),
	);

	return $messages;
}
//add_filter( 'term_updated_messages', 'class_updated_messages' );
