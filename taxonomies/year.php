<?php

/**
 * Registers the `year` taxonomy,
 * for use with 'lecture'.
 */
function year_init()
{
	register_taxonomy('year', array('lecture'), array(
		'hierarchical'      => false,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts',
		),
		'labels'            => array(
			'name'                       => __('Years', 'wpfab'),
			'singular_name'              => _x('Year', 'taxonomy general name', 'wpfab'),
			'search_items'               => __('Search Years', 'wpfab'),
			'popular_items'              => __('Popular Years', 'wpfab'),
			'all_items'                  => __('All Years', 'wpfab'),
			'parent_item'                => __('Parent Year', 'wpfab'),
			'parent_item_colon'          => __('Parent Year:', 'wpfab'),
			'edit_item'                  => __('Edit Year', 'wpfab'),
			'update_item'                => __('Update Year', 'wpfab'),
			'view_item'                  => __('View Year', 'wpfab'),
			'add_new_item'               => __('Add New Year', 'wpfab'),
			'new_item_name'              => __('New Year', 'wpfab'),
			'separate_items_with_commas' => __('Separate Years with commas', 'wpfab'),
			'add_or_remove_items'        => __('Add or remove Years', 'wpfab'),
			'choose_from_most_used'      => __('Choose from the most used Years', 'wpfab'),
			'not_found'                  => __('No Years found.', 'wpfab'),
			'no_terms'                   => __('No Years', 'wpfab'),
			'menu_name'                  => __('Years', 'wpfab'),
			'items_list_navigation'      => __('Years list navigation', 'wpfab'),
			'items_list'                 => __('Years list', 'wpfab'),
			'most_used'                  => _x('Most Used', 'year', 'wpfab'),
			'back_to_items'              => __('&larr; Back to Years', 'wpfab'),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'year',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	));
}

/**
 * Sets the post updated messages for the `year` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `year` taxonomy.
 */
function year_updated_messages($messages)
{

	$messages['year'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __('Year added.', 'wpfab'),
		2 => __('Year deleted.', 'wpfab'),
		3 => __('Year updated.', 'wpfab'),
		4 => __('Year not added.', 'wpfab'),
		5 => __('Year not updated.', 'wpfab'),
		6 => __('Years deleted.', 'wpfab'),
	);

	return $messages;
}
//add_filter( 'term_updated_messages', 'year_updated_messages' );
