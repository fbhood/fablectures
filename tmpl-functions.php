<?php

/* Custom function to filter the single_templates */
function fabwp_single_lecture_template($single)
{

    global $post;
    /* Checks for single template by post type */
    if ($post->post_type == 'lecture') {

        if (wp_get_theme() == "Twenty Twenty" ||  get_template_directory() !== get_stylesheet_directory()) {
            if (file_exists(plugin_dir_path(__FILE__) . 'templates/2020/single-lecture.php')) {
                return  plugin_dir_path(__FILE__) . 'templates/2020/single-lecture.php';
            }
        } elseif (wp_get_theme() == "Twenty Nineteen" ||  get_template_directory() !== get_stylesheet_directory()) {

            if (file_exists(plugin_dir_path(__FILE__) . 'templates/2019/single-lecture.php')) {
                return  plugin_dir_path(__FILE__) . 'templates/2019/single-lecture.php';
            }
        } else {
            if (file_exists(plugin_dir_path(__FILE__) . 'templates/single-lecture.php')) {
                return  plugin_dir_path(__FILE__) . 'templates/single-lecture.php';
            }
        }
    }

    return $single;
}
add_filter('single_template', 'fabwp_single_lecture_template');
