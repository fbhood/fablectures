<?php

/**
 * Plugin Name:     Fab Lecture Postypes And Taxonomies
 * Plugin URI:      plugins.fabiopacifici.com/fablectures
 * Description:     Adds Lecture PostType, Class and Year Taxonomies to your Wordpress website
 * Author:          Fabio Pacific
 * Author URI:      https://fabiopacifici.com
 * Text Domain:     fablecture
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Fab_Custom_Postypes_And_Taxo
 */

// Includes
include('activate.php');
include('deactivate.php');

// Actions and Filters

/* Lecture Post Type */
add_action('init', 'lecture_init');
add_filter('post_updated_messages', 'lecture_updated_messages');
/* Class taxonomy */
add_action('init', 'class_init');
add_filter('term_updated_messages', 'class_updated_messages');
/* Year Taxonomy */
add_action('init', 'year_init');
add_filter('term_updated_messages', 'year_updated_messages');

include('tmpl-functions.php');
