<?php

function fab_lecture_setup_post_type()
{
    # Register our custom post-type and taxo here
    include('post-types/lecture.php');
    include('taxonomies/class.php');
    include('taxonomies/year.php');
}
add_action('init', 'fab_lecture_setup_post_type');

function fab_lecture_install()
{
    // trigger our function that registers the post types
    fab_lecture_setup_post_type();

    // clear permalinks after hte post type has been registered
    flush_rewrite_rules();
}

register_activation_hook(__FILE__, 'fab_lecture_install');
