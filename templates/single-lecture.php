<?php
get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main" style="padding-left: 2rem; padding-right: 2rem;">

			<?php
			/* Start the Loop */
			while (have_posts()) : the_post();
				if (has_post_thumbnail()) {
					the_post_thumbnail('full');
				}
				the_taxonomies();
				the_title('<h1 class="entry-title">', '</h1>');
				the_content();

				// If comments are open or we have at least one comment, load up the comment template.
				if (comments_open() || get_comments_number()) :
					comments_template();
				endif;

				the_post_navigation();

			endwhile; // End of the loop.
			?>
		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();
