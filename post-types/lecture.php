<?php

/**
 * Registers the `lecture` post type.
 */
function lecture_init()
{
	register_post_type('lecture', array(
		'labels'                => array(
			'name'                  => __('Lectures', 'fab-custom-postypes-and-taxo'),
			'singular_name'         => __('Lecture', 'fab-custom-postypes-and-taxo'),
			'all_items'             => __('All Lectures', 'fab-custom-postypes-and-taxo'),
			'archives'              => __('Lecture Archives', 'fab-custom-postypes-and-taxo'),
			'attributes'            => __('Lecture Attributes', 'fab-custom-postypes-and-taxo'),
			'insert_into_item'      => __('Insert into Lecture', 'fab-custom-postypes-and-taxo'),
			'uploaded_to_this_item' => __('Uploaded to this Lecture', 'fab-custom-postypes-and-taxo'),
			'featured_image'        => _x('Featured Image', 'lecture', 'fab-custom-postypes-and-taxo'),
			'set_featured_image'    => _x('Set featured image', 'lecture', 'fab-custom-postypes-and-taxo'),
			'remove_featured_image' => _x('Remove featured image', 'lecture', 'fab-custom-postypes-and-taxo'),
			'use_featured_image'    => _x('Use as featured image', 'lecture', 'fab-custom-postypes-and-taxo'),
			'filter_items_list'     => __('Filter Lectures list', 'fab-custom-postypes-and-taxo'),
			'items_list_navigation' => __('Lectures list navigation', 'fab-custom-postypes-and-taxo'),
			'items_list'            => __('Lectures list', 'fab-custom-postypes-and-taxo'),
			'new_item'              => __('New Lecture', 'fab-custom-postypes-and-taxo'),
			'add_new'               => __('Add New', 'fab-custom-postypes-and-taxo'),
			'add_new_item'          => __('Add New Lecture', 'fab-custom-postypes-and-taxo'),
			'edit_item'             => __('Edit Lecture', 'fab-custom-postypes-and-taxo'),
			'view_item'             => __('View Lecture', 'fab-custom-postypes-and-taxo'),
			'view_items'            => __('View Lectures', 'fab-custom-postypes-and-taxo'),
			'search_items'          => __('Search Lectures', 'fab-custom-postypes-and-taxo'),
			'not_found'             => __('No Lectures found', 'fab-custom-postypes-and-taxo'),
			'not_found_in_trash'    => __('No Lectures found in trash', 'fab-custom-postypes-and-taxo'),
			'parent_item_colon'     => __('Parent Lecture:', 'fab-custom-postypes-and-taxo'),
			'menu_name'             => __('Lectures', 'fab-custom-postypes-and-taxo'),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array('title', 'editor'),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-book-alt',
		'show_in_rest'          => true,
		'rest_base'             => 'lecture',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
		'taxonomies'			=> array('category', 'post_tag')
	));
}
add_action('init', 'lecture_init');

/**
 * Sets the post updated messages for the `lecture` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `lecture` post type.
 */
function lecture_updated_messages($messages)
{
	global $post;

	$permalink = get_permalink($post);

	$messages['lecture'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf(__('Lecture updated. <a target="_blank" href="%s">View Lecture</a>', 'fab-custom-postypes-and-taxo'), esc_url($permalink)),
		2  => __('Custom field updated.', 'fab-custom-postypes-and-taxo'),
		3  => __('Custom field deleted.', 'fab-custom-postypes-and-taxo'),
		4  => __('Lecture updated.', 'fab-custom-postypes-and-taxo'),
		/* translators: %s: date and time of the revision */
		5  => isset($_GET['revision']) ? sprintf(__('Lecture restored to revision from %s', 'fab-custom-postypes-and-taxo'), wp_post_revision_title((int) $_GET['revision'], false)) : false,
		/* translators: %s: post permalink */
		6  => sprintf(__('Lecture published. <a href="%s">View Lecture</a>', 'fab-custom-postypes-and-taxo'), esc_url($permalink)),
		7  => __('Lecture saved.', 'fab-custom-postypes-and-taxo'),
		/* translators: %s: post permalink */
		8  => sprintf(__('Lecture submitted. <a target="_blank" href="%s">Preview Lecture</a>', 'fab-custom-postypes-and-taxo'), esc_url(add_query_arg('preview', 'true', $permalink))),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf(
			__('Lecture scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Lecture</a>', 'fab-custom-postypes-and-taxo'),
			date_i18n(__('M j, Y @ G:i', 'fab-custom-postypes-and-taxo'), strtotime($post->post_date)),
			esc_url($permalink)
		),
		/* translators: %s: post permalink */
		10 => sprintf(__('Lecture draft updated. <a target="_blank" href="%s">Preview Lecture</a>', 'fab-custom-postypes-and-taxo'), esc_url(add_query_arg('preview', 'true', $permalink))),
	);

	return $messages;
}
//add_filter('post_updated_messages', 'lecture_updated_messages');
